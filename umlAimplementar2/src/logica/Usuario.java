package logica;

import java.util.ArrayList;
import java.util.Date;

public class Usuario {
	private int id;
	private String nombre;
	private Date fechaNac;
	private ArrayList<Hospital> hosp;

	public Usuario(int id, String nombre, Date fechaNac, Hospital hosp) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fechaNac = fechaNac;
		this.hosp = new ArrayList<>();
		this.hosp.add(hosp);
	}
	
	public Hospital getHosp(int id) {
		int i = 0;
		while ((i < this.hosp.size()) && (id != this.hosp.get(i).getId())) {
			i++;
		}
		
		if (i < this.hosp.size()) {
			return this.hosp.get(i);
		} else {
			return null;
		}
	}
	
	public int cantHosps() {
		return this.hosp.size();
	}
	
	public ArrayList<Hospital> getHosps() {
		return this.hosp;
	}

	public void addHosp(Hospital hosp) {
		this.hosp.add(hosp);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	
	public int calcularSueldo() {
		return 1;
	}
	
	
}
