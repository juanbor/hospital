package logica;

import java.util.Date;

public class Medico extends Usuario {
	private String especialidad;
	private int sueldo;	
	
	public Medico(int id, String nombre, Date fechaNac, String especialidad, int sueldo, Hospital hosp) {
		super(id, nombre, fechaNac, hosp);
		this.especialidad = especialidad;
		this.sueldo = sueldo;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	
	public int calcularSueldo() {
		return sueldo;
	}
	
	
}
