package logica;

import java.util.Date;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Manejador man = new Manejador();
		
		man.altaHospital("Mercedes 1296", true, new Date());
		man.altaHospital("Canelones 1162", false, new Date());
		man.altaHospital("Br. Artigas 1152", true, new Date());

		man.altaMedico("Juan", new Date(), "Oftalmologia", 20000, 0);
		man.altaMedico("Ricardo", new Date(), "Oftalmologia", 12000, 0);
		man.altaMedico("Pedro", new Date(), "Nuerologia", 50000, 1);
		man.altaMedico("Pablo", new Date(), "Anatomia Patologica", 60000, 1);
		
		man.altaCliente("Carlos", new Date(), 12312, 1);
		
		man.altaAdministrativo("Carlos", new Date(), "Contabilidad", 12413, 2);
		man.altaAdministrativo("Fernando", new Date(), "Contabilidad", 14413, 2);
		man.altaAdministrativo("Rodrigo", new Date(), "Informatica", 30943, 2);
		man.altaAdministrativo("Pablo", new Date(), "Mantenimiento", 14413, 2);
		
		man.imprimirEstatusHospitales();
		man.imprimirEstatusUsuarios();
		man.imprimirEstatusGens();
		
		man.bajaAdministrativo(5);
		man.bajaAdministrativo(6);
		man.bajaCliente(4);
		man.bajaMedico(1);
		man.bajaMedico(2);
		man.bajaMedico(3);
		
		man.imprimirEstatusHospitales();
		man.imprimirEstatusUsuarios();
		man.imprimirEstatusGens();
		
		
	}

}
