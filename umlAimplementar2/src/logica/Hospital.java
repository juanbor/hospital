package logica;

import java.util.ArrayList;
import java.util.Date;

public class Hospital {
	private int id;
	private String direccion;
	private boolean servicioEme;
	private Date fechaInau;
	private ArrayList<Usuario> users;
	
	public Hospital(int id, String direccion, boolean servicioEme, Date fechaInau) {
		super();
		this.id = id;
		this.direccion = direccion;
		this.servicioEme = servicioEme;
		this.fechaInau = fechaInau;
		this.users = new ArrayList<>();
	}
	
	public Usuario getUser(int id) {
		int i = 0;
		while ((i < this.users.size()) && (id != this.users.get(i).getId())) {
			i++;
		}
		
		if (i < this.users.size()) {
			return this.users.get(i);
		} else {
			return null;
		}
	}

	public void addUsuario(Usuario user) {
		this.users.add(user);
	}
	
	public void bajaUsuario(int idU) {
		int i = 0;
		while ((i < users.size()) && (idU != users.get(i).getId())) {
			i++;
		}
		
		if (i < users.size()) {
			users.remove(i);
		}
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public boolean isServicioEme() {
		return servicioEme;
	}
	
	public void setServicioEme(boolean servicioEme) {
		this.servicioEme = servicioEme;
	}
	
	public Date getFechaInau() {
		return fechaInau;
	}
	
	public void setFechaInau(Date fechaInau) {
		this.fechaInau = fechaInau;
	}
	
	public int cantMedicos() {
		int i = 0;
		int contador = 0;
		while (i < this.users.size()) {
			try {
				Medico med = (Medico) this.users.get(i);
				contador++;
			} catch (ClassCastException e) {
				
			}
			i++;
		}
		return contador;
	}
	
	public int cantAdministr() {
		int i = 0;
		int contador = 0;
		while (i < this.users.size()) {
			try {
				Administrativo med = (Administrativo) this.users.get(i);
				contador++;
			} catch (ClassCastException e) {
				
			}
			i++;
		}
		return contador;
	}
	
	public int cantCliente() {
		int i = 0;
		int contador = 0;
		while (i < this.users.size()) {
			try {
				Cliente cli = (Cliente) this.users.get(i);
				contador++;
			} catch (ClassCastException e) {
				
			}
			i++;
		}
		return contador;
	}
	
	public int cantEspecialidades() {
		int i = 0;
		ArrayList<String> esp = new ArrayList<>();
		
		while (i < this.users.size()) {
			try {
				Medico med = (Medico) this.users.get(i);
				if (!esp.contains(med.getEspecialidad()))
					esp.add(med.getEspecialidad());
			} catch (ClassCastException e) {
				
			}
			i++;
		}
		return esp.size();
	}
	
	public int calcularLiquidacion() {
		int i = 0;
		int contador = 0;
		while (i < this.users.size()) {
			contador += this.users.get(i).calcularSueldo();
			i++;
		}
		return contador;
	}
	
	
}
